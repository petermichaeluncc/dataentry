Template.testgraphworkcell.created = function(){
  var start= moment().format("2000-01-01 HH:mm:ss.SSS")
 Session.set("startgraph", start);
  var end= moment().format("2100-01-01 HH:mm:ss.SSS")
    Session.set("endgraph", end);
//Meteor.subscribe('dataentriesnames', Session.get("startgraph"),Session.get("endgraph"));


};

Template.testgraphworkcell.rendered = function(){
  Session.set("workcenterNameSearch","1")
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });
  /*function doSomething() {
    $('select').material_select();
}
doSomething();
setInterval(doSomething, 9000);
*/
};

Template.testgraphworkcell.events({



 'change #start': function (event, template) {

console.log("this is the start")


// var length=Number(event.currentTarget.value.length)
 var start=$( "#start" ).val();
  var teststart= moment(start).format("YYYY-MM-DD")

  console.log("start in event" + teststart)
//  console.log("this is the length of numbers "+ start.replace(/[^0-9]/g,"").length)
  //length=length.replace(/[^0-9]/g,"").length

 Session.set("startgraph", teststart);


 },
 'change #end': function (event, template) {



// var length=Number(event.currentTarget.value.length)
       var end=$( "#end" ).val();
var testend= moment(end).format("YYYY-MM-DD")
console.log("This is the end " + testend)
   //var end= moment(event.currentTarget.value).format("YYYY-MM-DD HH:mm:ss.SSS")
    console.log("end in event" + testend)

   Session.set("endgraph", testend);


 },
 'click .modaltrigger': function (event, template) {



 },
 'change .department': function (event, template) {

   console.log("there was a change in the department")
  var department= $('[name=department]').val();
  console.log("this is the selected department "+department)
  //call server method to retrieve the
  departmentReturn()


 }


});
Template.testgraphworkcell.topGenresChart = function() {

    //Here I need to have a server side fxn that  I call that returns
    //the average for permanent and another for temp

    console.log("This is the graph name " + Session.get("graphname"))
    console.log("this is the workcenter name " + Session.get("workcenterNameSearch"))
//based on the count I need to determine which data set to ship over to this graph
//
/*
Basically I will

*/

var both=ReactiveMethod.call('averageworkcenter',Session.get("workcenterNameSearch"),Session.get("startgraph"),Session.get("endgraph"))
var shift1=both[0]
var shift2=both[1]
var shift3=both[2]


if (typeof shift1=="number"||typeof shift2=="number"||typeof shift3=="number")
{Session.set("loading",0)
    var datatest=[
                ['Shift1',  shift1],
                ['Shift2',   shift2],
                ['Shift3',   shift3]


            ]
    return {
        chart: {
            renderTo: 'container',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 25,
                depth: 32,
                viewDistance: 40
            },
            style: {
                    color: 'black',
                    fontSize:'25px'
                }
        },
        title: {
            text: "Workcenter shift comparison"
        },
         xAxis: {
            categories: ['Shift 1', 'Shift 2','Shift 3'],
            labels: {
                style: {
                    color: 'black',
                    fontSize:'25px'
                }
            }
        },
        yAxis: {
            title: {
                text: 'Productivity'
            },
            labels: {
                 formatter: function () {
                    return this.value + '%';
                },
                style: {
                    color: 'black',
                    fontSize:'25px'
                }
            },
            stackLabels: {
                enabled: true,
                align: 'center',
                formatter: function () {
                    return this.y + '%';
                }
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{this.y}</b> %',
            shared: true
        },
        plotOptions: {
            column: {
                depth: 25
            },
         series: {
                dataLabels: {
                    align: 'center',
                    enabled: true,
                    rotation: 0,
                    x: 2,
                    y: -10,
                    formatter: function () {

                            return this.y + '%';
                       // return Highcharts.numberFormat(this.y,1);

                }
                },

            }
        },
        series: [{
            color: '#9fa8da',
            data: datatest
        }]




    };

function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }
        showValues();

    }
    else
    {
        Session.set("loading",1)
    }
};




Template.testgraphworkcell.helpers({
    timeframe: function () {
/*
So here I need to show the user the time frame this is going on
I need to grab the earliest entry timestamp so the first one
and the last entry time stamp

*/
/*
I'll need to contact a meteor method

*/


},
workcenters: function(){


return Session.get("workcenters")

}
})
