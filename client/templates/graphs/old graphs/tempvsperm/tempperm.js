

  Template.testgraphnew.created = function(){
    var start= moment().format("2000-01-01 HH:mm:ss.SSS")
   Session.set("startgraph", start);
    var end= moment().format("2100-01-01 HH:mm:ss.SSS")
      Session.set("endgraph", end);
  //Meteor.subscribe('dataentriesnames', Session.get("startgraph"),Session.get("endgraph"));


  };

  Template.testgraphnew.rendered = function(){
    Session.set("workcenterNameSearch","1")
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });


  };

 Template.testgraphnew.events({



   'change #start': function (event, template) {

 console.log("this is the start")


  // var length=Number(event.currentTarget.value.length)
   var start=$( "#start" ).val();
    var teststart= moment(start).format("YYYY-MM-DD")

    console.log("start in event" + teststart)
  //  console.log("this is the length of numbers "+ start.replace(/[^0-9]/g,"").length)
    //length=length.replace(/[^0-9]/g,"").length

   Session.set("startgraph", teststart);


   },
   'change #end': function (event, template) {



  // var length=Number(event.currentTarget.value.length)
         var end=$( "#end" ).val();
  var testend= moment(end).format("YYYY-MM-DD")
 console.log("This is the end " + testend)
     //var end= moment(event.currentTarget.value).format("YYYY-MM-DD HH:mm:ss.SSS")
      console.log("end in event" + testend)

     Session.set("endgraph", testend);


   }


 });

Template.testgraphnew.topGenresChart = function() {

	//Here I need to have a server side fxn that  I call that returns
	//the average for permanent and another for temp

    console.log("This is the graph name " + Session.get("graphname"))
//based on the count I need to determine which data set to ship over to this graph
//
//So I need to ship in start and end times from the input boxes
var both=ReactiveMethod.call('average',Session.get("department1"),Session.get("startgraph"),Session.get("endgraph"))
var temp=both[0]
var permanent=both[1]

//var temp=ReactiveMethod.call('tempaverage')
//var permanent=ReactiveMethod.call('permanentaverage')
if (typeof permanent=="number")
{Session.set("loading",0)
	var datatest=[
                ['Temp',  temp],
                ['Permanent',       permanent]


            ]
    return {
        chart: {
            renderTo: 'container',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 25,
                depth: 32,
                viewDistance: 40
            },
            style: {
                    color: 'black',
                    fontSize:'25px'
                }
        },
        title: {
            text: Session.get("graphname")
        },
         xAxis: {
            categories: ['Temp', 'Permanent'],
            labels: {
                style: {
                    color: 'black',
                    fontSize:'25px'
                }
            }
        },
        scrollbar: {
            enabled: true
        },
        yAxis: {
            title: {
                text: 'Productivity'
            },
            labels: {
                 formatter: function () {
                    return this.value + '%';
                },
                style: {
                    color: 'black',
                    fontSize:'15px'
                }
            },
            stackLabels: {
                enabled: false,
                align: 'center',
                formatter: function () {
                    return this.y + '%';
                }
            }
        },

        plotOptions: {
            column: {
                depth: 25
            },
         series: {


                dataLabels: {
                    align: 'center',
                    enabled: false,
                    rotation: 0,

                    x: 2,
                    y: -10,
                    formatter: function () {

                            return this.y + '%';
                       // return Highcharts.numberFormat(this.y,1);

                }
                },

            }
        },
        series: [{
            color: '#9fa8da',
            data: datatest
        }]




    };

function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }
        showValues();

    }
    else
    {
        Session.set("loading",1)
    }
};




Template.testgraphnew.helpers({
    timeframe: function () {
/*
So here I need to show the user the time frame this is going on
I need to grab the earliest entry timestamp so the first one
and the last entry time stamp

*/
/*
I'll need to contact a meteor method

*/
 var start=Session.get("startgraph")
 start= moment(start).format("YYYY-MM-DD")
var amount="Start date: ".concat(start)
// Materialize.toast(amount, 20000, 'blue')
var enddate="End date: "
var end=Session.get("endgraph")
 end= moment(end).format("YYYY-MM-DD")
enddate=enddate.concat(end)
 //Materialize.toast(enddate, 20000, 'blue')


}
})
