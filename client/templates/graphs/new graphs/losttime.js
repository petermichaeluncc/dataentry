
    Template.losttime.created = function(){
      var start= moment().format("2000-01-01 HH:mm:ss.SSS")
     Session.set("startgraph", start);
      var end= moment().format("2100-01-01 HH:mm:ss.SSS")
        Session.set("endgraph", end);
    //Meteor.subscribe('dataentriesnames', Session.get("startgraph"),Session.get("endgraph"));


    };

    Template.losttime.rendered = function(){


//$('select').material_select();

      Session.set("workcenterNameSearch","1")
      $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
      });


    };

 Template.losttime.events({



     'change #start': function (event, template) {

   console.log("this is the start")


    // var length=Number(event.currentTarget.value.length)
     var start=$( "#start" ).val();
      var teststart= moment(start).format("YYYY-MM-DD")

      console.log("start in event" + teststart)
    //  console.log("this is the length of numbers "+ start.replace(/[^0-9]/g,"").length)
      //length=length.replace(/[^0-9]/g,"").length

     Session.set("startgraph", teststart);


     },
     'change #end': function (event, template) {



    // var length=Number(event.currentTarget.value.length)
           var end=$( "#end" ).val();
    var testend= moment(end).format("YYYY-MM-DD")
console.log("This is the end " + testend)
       //var end= moment(event.currentTarget.value).format("YYYY-MM-DD HH:mm:ss.SSS")
        console.log("end in event" + testend)

       Session.set("endgraph", testend);


     },
     'click .modaltrigger': function (event, template) {
//$('select').material_select();
departmentReturn()

     },
     'change .department': function (event, template) {

       console.log("there was a change in the department")
      var department= $('[name=department]').val();
      console.log("this is the selected department "+department)
      //call server method to retrieve the
      departmentReturn()


     },


});
Template.losttime.topGenresChart = function() {

    //Here I need to have a server side fxn that  I call that returns
    //the average for permanent and another for temp

    console.log("This is the graph name " + Session.get("graphname"))
    console.log("this is the workcenter name " + Session.get("workcenterNameSearch"))


var start = moment().format("SS");
console.log("this is the start "+start)



  var lostTime=ReactiveMethod.call('averageworkcenterLostTime',Session.get("workcenterNameSearch"),Session.get("startgraph"),Session.get("endgraph"))
//
if (typeof lostTime=="object")
{console.log("this is the last time of lateStart "+lostTime["lateStartshift1"])
console.log("this is the typeof last time of lateStart "+ typeof lostTime["lateStartshift1"])
  console.log("typeof lost time is object")
  Session.set("loading",0)
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });

}
else {
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });

  Session.set("loading",1)
}
console.log("this is lost time "+lostTime)
console.log("this is the typeof losttime "+typeof lostTime)


//

if (typeof lostTime=="object")
{
    var datatest=[
                ['Late Start S1',  lostTime["lateStartshift1"]],
                ['Late Start S2',   lostTime["lateStartshift2"]],
                ['Late Start S3',   lostTime["lateStartshift3"]],
                ['Short Operators S1',  lostTime["shortOperatorsshift1"]],
                ['Short Operators S2',   lostTime["shortOperatorsshift2"]],
                ['Short Operators S3',   lostTime["shortOperatorsshift3"]],
                ['Training S1',  lostTime["trainingshift1"]],
                ['Training S2',   lostTime["trainingshift2"]],
                ['Training S3',   lostTime["trainingshift3"]],
                ['Parts Shortage S1',  lostTime["partsShortageshift1"]],
                ['Parts Shortage S2',   lostTime["partsShortageshift2"]],
                ['Parts Shortage S3',   lostTime["partsShortageshift3"]],
                ['Getting Parts S1',  lostTime["gettingPartsshift1"]],
                ['Getting Parts S2',   lostTime["gettingPartsshift2"]],
                ['Getting Parts S3',   lostTime["gettingPartsshift3"]],
                ['Quality Issue S1',  lostTime["qualityIssueshift1"]],
                ['Quality Issue S2',   lostTime["qualityIssueshift2"]],
                ['Quality Issue S3',   lostTime["qualityIssueshift3"]],
                ['Other S1',  lostTime["othershift1"]],
                ['Other S2',   lostTime["othershift2"]],
                ['Other S3',   lostTime["othershift3"]]

            ]
    return {
        chart: {
            renderTo: 'container',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 25,
                depth: 32,
                viewDistance: 40
            },
            style: {
                    color: 'black',
                    fontSize:'10px'
                }
        },
        title: {
            text: "Workcenter Lost Time"
        },
         xAxis: {
           scrollbar: {
             enabled: true
                  },
            categories: ['Late Start S1', 'Late Start S2','Late Start S3',"Short Operators S1","Short Operators S2","Short Operators S3"],


            labels: {
                style: {
                    color: 'black',
                    fontSize:'10px'
                }
            }
        },

        yAxis: {
            title: {
                text: 'Minutes'
            },
            labels: {
                 formatter: function () {
                    return this.value + '';
                },
                style: {
                    color: 'black',
                    fontSize:'25px'
                }
            },
            stackLabels: {
                enabled: true,
                align: 'center',
                formatter: function () {
                    return this.y + '';
                }
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{this.y}</b> ',
            shared: true
        },
        plotOptions: {
            column: {
                depth: 25
            },
         series: {
                dataLabels: {
                    align: 'center',
                    
                    enabled: true,
                    rotation: 0,
                    x: 2,
                    y: -10,
                    formatter: function () {

                            return this.y + '';
                       // return Highcharts.numberFormat(this.y,1);

                }
                },

            },
            bar: {
           dataLabels: {
               enabled: true
           }
       }
        },
        series: [{
            color: '#9fa8da',
            data: datatest
        }]




    };

function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }
        showValues();



    //    Session.set("loading",1)
}
};





Template.losttime.helpers({
    timeframe: function () {
/*
So here I need to show the user the time frame this is going on
I need to grab the earliest entry timestamp so the first one
and the last entry time stamp

*/
/*
I'll need to contact a meteor method

*/


},

workcenters: function(){
//$('select').material_select();

return Session.get("workcenters")

}
})
